const Header = ({ info }) => (
  <header>
    <h1>{info.name}</h1>
    <div>{info.hours}</div>
    <div>{info.address}</div>
    <div>{info.phone}</div>
    <div>{info.tagline}</div>
    <div className="phrase">
      <div>{info.phrase.greek}</div>
      <div>{info.phrase.english}</div>
    </div>
  </header>
);

export default Header;
