const Breads = ({ breads }) => {
  if (!breads) {
    return null;
  }

  return <ul>{breads.map((item, index) => <li key={index}>{item}</li>)}</ul>;
};

export default Breads;
