const MenuItems = ({ menuItem }) => (
  <ul>
    {menuItem["menu-items"].map((item, i) => (
      <li key={i}>
        <div>{item.name}</div>
        <div>{item.price}</div>
        {item.description ? <div>{item.description}</div> : ""}
      </li>
    ))}
  </ul>
);

export default MenuItems;
