const CategoryInfo = ({ info }) => {
  if (!info) {
    return null;
  }

  return <p>{info}</p>;
};

export default CategoryInfo;
