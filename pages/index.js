import data from "../deli-menu.json";
import Header from "../components/Header.js";
import CategoryInfo from "../components/CategoryInfo.js";
import MenuItems from "../components/MenuItems.js";
import Breads from "../components/Breads.js";

const page = () => (
  <div>
    <Header info={data.info} />
    {data.categories.map((item, index) => (
      <section key={index}>
        <h2>{item.category}</h2>
        <CategoryInfo info={item.info} />
        <MenuItems menuItem={item} />
        <Breads breads={item.breads} />
      </section>
    ))}
  </div>
);

export default page;
